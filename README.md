# _Simple Lightbox_

A small lightweight script for simple lightboxes

## Project Setup

add the following to your projects bower.json file dependencies
```
	"simple-lightbox":"https://bitbucket.org/tlherr/simple-lightbox.git"
```
then run:
```
bower install
```
