;(function($) {
	var defaults, methods;
				
	defaults = {
		next: '',		
		prev: '',
		tpl: {
			img: '<div id="sl-overlay"></div><div id="sl-lightbox"><div id="sl-lightbox-inner"><div id="sl-lightbox-scroll"><img /></div></div><a href="#" id="sl-lightbox-close">Close</a></div>',
			html: '<div id="sl-overlay"></div><div id="sl-lightbox"><div id="sl-lightbox-inner"></div><a href="#" id="sl-lightbox-close">Close</a></div>',
			htmls: '<div id="sl-overlay"></div><div id="sl-lightbox"><div id="sl-lightbox-inner"><div id="sl-lightbox-scroll"></div></div><a href="#" id="sl-lightbox-close">Close</a></div>',
		},
		scroller: null
	};
	
	methods = {
		init: function (options) {
			var opts;
			if (typeof $.fn.simpleLightbox.opts != "object") {
				options = options || {};
				opts = $.extend({}, $.fn.simpleLightbox.defaults, options);
	
				// Store the options
				$.fn.simpleLightbox.opts = opts;
			}
		},
		showImage: function (imgURL, scrollOffset) {
			var scroller, wRatio, imgWidth, halfWidth, scrollTo, tpl;
			
			tpl = $.fn.simpleLightbox.opts.tpl.img; 
						
			// Add the image lightbox content to the page and hide it
			$(tpl).appendTo('body').hide();
			
			// Add click events to the overlay to hide
			$('div#sl-overlay').click(function() {
				$.fn.simpleLightbox('hide');
			});
			
			$('a#sl-lightbox-close').click(function() {
				$.fn.simpleLightbox('hide');
			});
			
			$('div#sl-lightbox img')
				.attr('src', imgURL)
				.load(function() {
					if ($.fn.simpleLightbox.scroller != null) {
						$.fn.simpleLightbox.scroller.destroy();
						$.fn.simpleLightbox.scroller = null
					}
					
					wRatio = 648 / this.height;
					imgWidth = wRatio * this.width;
					
					scrollTo = (scrollOffset * imgWidth) - 452;
					if (scrollTo > (imgWidth - 904)) {
						scrollTo = (imgWidth - 904);
					} else if (scrollTo < 0) {
						scrollTo = 0;
					}
					
					$(this).css('height', '648px').css('width', imgWidth + 'px');
					$.fn.simpleLightbox.scroller = new iScroll('sl-lightbox-scroll', {fadeScrollbar: false, hideScrollbar: false});
					// Fade in the overlay and image
					$('div#sl-overlay').fadeIn();
					$('div#sl-lightbox').fadeIn();
					
					// Set this to refresh after things have started fading
					setTimeout(function() {
						$.fn.simpleLightbox.scroller.refresh();
						$.fn.simpleLightbox.scroller.scrollTo(-scrollTo, 0, 0, false);
					}, 100);
				});
		},
		showContent: function (content, scroller, callback) {
			var tpl;
			if (scroller) {
				tpl = $.fn.simpleLightbox.opts.tpl.htmls; 	
			} else {
				tpl = $.fn.simpleLightbox.opts.tpl.html; 
			}
			
			// Add the image lightbox content to the page and hide it
			$(tpl).appendTo('body').hide();
			
			// Add click events to the overlay to hide
			$('div#sl-overlay').click(function() {
				$.fn.simpleLightbox('hide');
				if (typeof(callback) == typeof(Function)) {
					callback();
				}
			});
			
			$('a#sl-lightbox-close').click(function() {
				$.fn.simpleLightbox('hide');
				if (typeof(callback) == typeof(Function)) {
					callback();
				}
			});
			
			if (scroller) {
				$('div#sl-lightbox-scroll').html(content);
			} else {
				$('div#sl-lightbox-inner').html(content);
			}
				

			if (scroller) {
				$.fn.simpleLightbox.scroller = new iScroll('sl-lightbox-inner', {fadeScrollbar: false, hideScrollbar: false});
				
				// Fade in the overlay and image
				$('div#sl-overlay').fadeIn();
				$('div#sl-lightbox').fadeIn();
				
				// Set this to refresh after things have started fading
				setTimeout(function() {
					$.fn.simpleLightbox.scroller.refresh();
				}, 100);
			} else {
				// Fade in the overlay and image
				$('div#sl-overlay').fadeIn();
				$('div#sl-lightbox').fadeIn();
			}
			
		},
		hide: function () {
			$('div#sl-overlay').fadeOut();
			$('div#sl-lightbox').fadeOut();

			setTimeout(function() {
				if ($.fn.simpleLightbox.scroller != null) {
					$.fn.simpleLightbox.scroller.destroy();
					$.fn.simpleLightbox.scroller = null
				}
				
				$('div#sl-overlay').remove();
				$('div#sl-lightbox').remove();
			}, 500);
		}
	};
	
	$.fn.simpleLightbox = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.simpleLightbox');
		}
	};
	
	$.fn.simpleLightbox.defaults = defaults;
})(jQuery);